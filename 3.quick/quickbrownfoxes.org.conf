resolver 127.0.0.11 valid=30s; # Docker internal DNS resolver

# Upstream definitions
upstream nginx_exporter {
    zone nginx_exporter 64k;
    server nginx-exporter:9113 resolve;
}

upstream webhook {
    zone webhook 64k;
    server webhook:4131 resolve;
}

upstream code_server {
    zone code_server 64k;	
    server code-server resolve;
}

upstream code_server_yv {
    zone code_server_yv 64k;	
    server code-server-yv resolve;
}

upstream send_web_server {
    zone send_web_server 64k;
    server send-web-server resolve;
}

upstream neko {
    zone neko 64k;	
    server neko:8080 resolve;
}

upstream unciv {
    zone unciv 64k;	
    server unciv:8080 resolve;
}

upstream plex {
    zone unciv 64k;
    server plex:32400 resolve;
}

server {
    listen [::]:443 ssl;
    listen 443 ssl;
    http2 on;
    server_name quickbrownfoxes.org;
    root /home/vincent/webapps/quickbrownfoxes;
    index index.html index.php;
    location = /.well-known/security.txt { return 200 "Contact: https://quickbrownfoxes.org/#contact \nContact: https://keybase.io/vincentchu37 \nEncryption: https://keybase.io/vincentchu37/pgp_keys.asc \nPreferred-Languages: en"; }
    location / {
        try_files $uri $uri/ /index.php$is_args$args;
    }
    location /metrics {
	auth_basic "metrics";
	auth_basic_user_file /home/vincent/webapps/quickbrownfoxes/htpasswd;
        proxy_pass http://nginx_exporter;
    }
    location ~ \.php$ {
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $request_filename;
        fastcgi_pass fpm:9000;
    }
    location ~ /\.git {
        return 404;
    }
    allow 173.245.48.0/20;
    allow 103.21.244.0/22;
    allow 103.22.200.0/22;
    allow 103.31.4.0/22;
    allow 141.101.64.0/18;
    allow 108.162.192.0/18;
    allow 190.93.240.0/20;
    allow 188.114.96.0/20;
    allow 197.234.240.0/22;
    allow 198.41.128.0/17;
    allow 162.158.0.0/15;
    allow 104.16.0.0/13;
    allow 104.24.0.0/14;
    allow 172.64.0.0/13;
    allow 131.0.72.0/22;

    allow 2400:CB00::/32;
    allow 2606:4700::/32;
    allow 2803:F800::/32;
    allow 2405:B500::/32;
    allow 2405:8100::/32;
    allow 2A06:98C0::/29;
    allow 2C0F:F248::/32;
    deny all;
    ssl_certificate /etc/letsencrypt/live/quickbrownfoxes.org/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/quickbrownfoxes.org/privkey.pem;
}

server {
    listen [::]:443 ssl;
    listen 443 ssl;
    http2 on;
    server_name r6.quickbrownfoxes.org;
    root /home/vincent/webapps/r6-strat-roulette;
    index index.html index.php;
    location = /.well-known/security.txt { return 200 "Contact: https://quickbrownfoxes.org/#contact \nContact: https://keybase.io/vincentchu37 \nEncryption: https://keybase.io/vincentchu37/pgp_keys.asc \nPreferred-Languages: en"; }
    location / {
        try_files $uri $uri/ /index.php$is_args$args;
    }
    location ~ \.php$ {
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $request_filename;
        fastcgi_pass fpm:9000;
    }
    location ~ /\.git {
        return 404;
    }
    allow 173.245.48.0/20;
    allow 103.21.244.0/22;
    allow 103.22.200.0/22;
    allow 103.31.4.0/22;
    allow 141.101.64.0/18;
    allow 108.162.192.0/18;
    allow 190.93.240.0/20;
    allow 188.114.96.0/20;
    allow 197.234.240.0/22;
    allow 198.41.128.0/17;
    allow 162.158.0.0/15;
    allow 104.16.0.0/13;
    allow 104.24.0.0/14;
    allow 172.64.0.0/13;
    allow 131.0.72.0/22;

    allow 2400:CB00::/32;
    allow 2606:4700::/32;
    allow 2803:F800::/32;
    allow 2405:B500::/32;
    allow 2405:8100::/32;
    allow 2A06:98C0::/29;
    allow 2C0F:F248::/32;
    deny all;
    ssl_certificate /etc/letsencrypt/live/quickbrownfoxes.org/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/quickbrownfoxes.org/privkey.pem;
}

server {
    listen [::]:443 ssl;
    listen 443 ssl;
    http2 on;
    server_name deploy.quickbrownfoxes.org;
    location = /.well-known/security.txt { return 200 "Contact: https://quickbrownfoxes.org/#contact \nContact: https://keybase.io/vincentchu37 \nEncryption: https://keybase.io/vincentchu37/pgp_keys.asc \nPreferred-Languages: en"; }
    location = /robots.txt { return 200 "User-agent: *\nDisallow: /\n"; }
    location / {
        proxy_pass http://webhook;
    }
    location ~ /\.git {
        return 404;
    }
    allow 173.245.48.0/20;
    allow 103.21.244.0/22;
    allow 103.22.200.0/22;
    allow 103.31.4.0/22;
    allow 141.101.64.0/18;
    allow 108.162.192.0/18;
    allow 190.93.240.0/20;
    allow 188.114.96.0/20;
    allow 197.234.240.0/22;
    allow 198.41.128.0/17;
    allow 162.158.0.0/15;
    allow 104.16.0.0/13;
    allow 104.24.0.0/14;
    allow 172.64.0.0/13;
    allow 131.0.72.0/22;
    
    allow 2400:CB00::/32;
    allow 2606:4700::/32;
    allow 2803:F800::/32;
    allow 2405:B500::/32;
    allow 2405:8100::/32;
    allow 2A06:98C0::/29;
    allow 2C0F:F248::/32;
    deny all;
    ssl_certificate /etc/letsencrypt/live/quickbrownfoxes.org/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/quickbrownfoxes.org/privkey.pem;
}

server {
    listen [::]:443 ssl;
    listen 443 ssl;
    http2 on;
    server_name code.quickbrownfoxes.org;
    proxy_set_header X-Real-IP  $remote_addr; # pass on real client IP
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection "Upgrade";
    proxy_set_header Host $host;
    allow 173.245.48.0/20;
    allow 103.21.244.0/22;
    allow 103.22.200.0/22;
    allow 103.31.4.0/22;
    allow 141.101.64.0/18;
    allow 108.162.192.0/18;
    allow 190.93.240.0/20;
    allow 188.114.96.0/20;
    allow 197.234.240.0/22;
    allow 198.41.128.0/17;
    allow 162.158.0.0/15;
    allow 104.16.0.0/13;
    allow 104.24.0.0/14;
    allow 172.64.0.0/13;
    allow 131.0.72.0/22;

    allow 2400:CB00::/32;
    allow 2606:4700::/32;
    allow 2803:F800::/32;
    allow 2405:B500::/32;
    allow 2405:8100::/32;
    allow 2A06:98C0::/29;
    allow 2C0F:F248::/32;
    deny all;
    location = /.well-known/security.txt { return 200 "Contact: https://quickbrownfoxes.org/#contact \nContact: https://keybase.io/vincentchu37 \nEncryption: https://keybase.io/vincentchu37/pgp_keys.asc \nPreferred-Languages: en"; }
    location = /robots.txt { return 200 "User-agent: *\nDisallow: /\n"; }
    location / {
        proxy_pass https://code_server;
    }
    location ~ /\.git {
        return 404;
    }
    ssl_certificate /etc/letsencrypt/live/quickbrownfoxes.org/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/quickbrownfoxes.org/privkey.pem;
}

server {
    listen [::]:443 ssl;
    listen 443 ssl;
    http2 on;
    server_name code-yv.quickbrownfoxes.org;
    proxy_set_header X-Real-IP  $remote_addr; # pass on real client IP
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection "Upgrade";
    proxy_set_header Host $host;
    location = /.well-known/security.txt { return 200 "Contact: https://quickbrownfoxes.org/#contact \nContact: https://keybase.io/vincentchu37 \nEncryption: https://keybase.io/vincentchu37/pgp_keys.asc \nPreferred-Languages: en"; }
    location = /robots.txt { return 200 "User-agent: *\nDisallow: /\n"; }
    location / {
        proxy_pass https://code_server_yv;
    }
    location ~ /\.git {
        return 404;
    }
    allow 173.245.48.0/20;
    allow 103.21.244.0/22;
    allow 103.22.200.0/22;
    allow 103.31.4.0/22;
    allow 141.101.64.0/18;
    allow 108.162.192.0/18;
    allow 190.93.240.0/20;
    allow 188.114.96.0/20;
    allow 197.234.240.0/22;
    allow 198.41.128.0/17;
    allow 162.158.0.0/15;
    allow 104.16.0.0/13;
    allow 104.24.0.0/14;
    allow 172.64.0.0/13;
    allow 131.0.72.0/22;

    allow 2400:CB00::/32;
    allow 2606:4700::/32;
    allow 2803:F800::/32;
    allow 2405:B500::/32;
    allow 2405:8100::/32;
    allow 2A06:98C0::/29;
    allow 2C0F:F248::/32;
    deny all;
    ssl_certificate /etc/letsencrypt/live/quickbrownfoxes.org/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/quickbrownfoxes.org/privkey.pem;
}

server {
    listen [::]:443 ssl;
    listen 443 ssl;
    http2 on;
    server_name dev.quickbrownfoxes.org;
    root /home/vincent/code;
    index index.html index.php;
    location = /.well-known/security.txt { return 200 "Contact: https://quickbrownfoxes.org/#contact \nContact: https://keybase.io/vincentchu37 \nEncryption: https://keybase.io/vincentchu37/pgp_keys.asc \nPreferred-Languages: en"; }
    location / {
        try_files $uri $uri/ /index.php$is_args$args;
    }
    location ~ \.php$ {
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $request_filename;
        fastcgi_pass fpm:9000;
    }
    location ~ /\.git {
        return 404;
    }
    allow 173.245.48.0/20;
    allow 103.21.244.0/22;
    allow 103.22.200.0/22;
    allow 103.31.4.0/22;
    allow 141.101.64.0/18;
    allow 108.162.192.0/18;
    allow 190.93.240.0/20;
    allow 188.114.96.0/20;
    allow 197.234.240.0/22;
    allow 198.41.128.0/17;
    allow 162.158.0.0/15;
    allow 104.16.0.0/13;
    allow 104.24.0.0/14;
    allow 172.64.0.0/13;
    allow 131.0.72.0/22;

    allow 2400:CB00::/32;
    allow 2606:4700::/32;
    allow 2803:F800::/32;
    allow 2405:B500::/32;
    allow 2405:8100::/32;
    allow 2A06:98C0::/29;
    allow 2C0F:F248::/32;
    deny all;
    ssl_certificate /etc/letsencrypt/live/quickbrownfoxes.org/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/quickbrownfoxes.org/privkey.pem;
}
server {
    listen [::]:443 ssl;
    listen 443 ssl;
    http2 on;
    server_name send.quickbrownfoxes.org;
    #proxy_set_header X-Real-IP  $remote_addr; # pass on real client IP
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection "Upgrade";
    proxy_set_header Host $host;
    proxy_buffering off;
    location = /.well-known/security.txt { return 200 "Contact: https://quickbrownfoxes.org/#contact \nContact: https://keybase.io/vincentchu37 \nEncryption: https://keybase.io/vi
ncentchu37/pgp_keys.asc \nPreferred-Languages: en"; }
    location / {
      proxy_pass http://send_web_server;
    }
    satisfy all;
    #auth_basic "File Send";
    #auth_basic_user_file /home/vincent/sendhtpasswd;
    allow 173.245.48.0/20;
    allow 103.21.244.0/22;
    allow 103.22.200.0/22;
    allow 103.31.4.0/22;
    allow 141.101.64.0/18;
    allow 108.162.192.0/18;
    allow 190.93.240.0/20;
    allow 188.114.96.0/20;
    allow 197.234.240.0/22;
    allow 198.41.128.0/17;
    allow 162.158.0.0/15;
    allow 104.16.0.0/13;
    allow 104.24.0.0/14;
    allow 172.64.0.0/13;
    allow 131.0.72.0/22;

    allow 2400:CB00::/32;
    allow 2606:4700::/32;
    allow 2803:F800::/32;
    allow 2405:B500::/32;
    allow 2405:8100::/32;
    allow 2A06:98C0::/29;
    allow 2C0F:F248::/32;
    deny all;
    ssl_certificate /etc/letsencrypt/live/quickbrownfoxes.org/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/quickbrownfoxes.org/privkey.pem;
}

server {
    listen [::]:443 ssl;
    listen 443 ssl;
    http2 on;
    server_name browse.quickbrownfoxes.org;
    location / {
	proxy_pass http://neko;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";
        proxy_read_timeout 86400;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $remote_addr;
        proxy_set_header X-Forwarded-Host $host;
        proxy_set_header X-Forwarded-Port $server_port;
        proxy_set_header X-Forwarded-Protocol $scheme;
    }
    ssl_certificate /etc/letsencrypt/live/quickbrownfoxes.org/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/quickbrownfoxes.org/privkey.pem;
}

server {
    listen [::]:443 ssl;
    listen 443 ssl;
    http2 on;
    server_name unciv.quickbrownfoxes.org;
    location / {
        proxy_pass http://unciv;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";
        proxy_read_timeout 86400;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $remote_addr;
        proxy_set_header X-Forwarded-Host $host;
        proxy_set_header X-Forwarded-Port $server_port;
        proxy_set_header X-Forwarded-Protocol $scheme;
    }
    ssl_certificate /etc/letsencrypt/live/quickbrownfoxes.org/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/quickbrownfoxes.org/privkey.pem;
}

server {
    listen [::]:443 ssl;
    listen 443 ssl;
    http2 on;
    server_name plex.quickbrownfoxes.org;
    location = /.well-known/security.txt { return 200 "Contact: https://quickbrownfoxes.org/#contact \nContact: https://keybase.io/vincentchu37 \nEncryption: https://keybase.io/vi
ncentchu37/pgp_keys.asc \nPreferred-Languages: en"; }
    location / {
        proxy_pass http://plex;
    }
    #Forward real ip and host to Plex
	proxy_set_header Host $host;
	proxy_set_header X-Real-IP $remote_addr;
	#When using ngx_http_realip_module change $proxy_add_x_forwarded_for to '$http_x_forwarded_for,$realip_remote_addr'
	proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
	proxy_set_header X-Forwarded-Proto $scheme;
	proxy_set_header Sec-WebSocket-Extensions $http_sec_websocket_extensions;
	proxy_set_header Sec-WebSocket-Key $http_sec_websocket_key;
	proxy_set_header Sec-WebSocket-Version $http_sec_websocket_version;
    proxy_http_version 1.1;
	proxy_set_header Upgrade $http_upgrade;
	proxy_set_header Connection "Upgrade";
    proxy_redirect off;
    proxy_buffering off;
    ssl_certificate /etc/letsencrypt/live/quickbrownfoxes.org/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/quickbrownfoxes.org/privkey.pem;
}

